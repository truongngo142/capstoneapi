// render dssp
renderDSSP = (dssp) => {
  var contentHTML = "";
  for (i = 0; i < dssp.length; i++) {
    var sp = dssp[i];
    // sv là item của array DSSV
    var contentTr = `<div class="product-box">
                <img src="${sp.img}" alt="">
                <h4 class="product-title"> ${sp.name}</h4>
                <div class="d-flex bottom">
                    <span class="price">$${sp.price}</span>
                    <i class='bx bxs-cart-add add-cart' onclick = "addCart(${sp.id})"></i>
                </div>
            </div>`;
    contentHTML += contentTr;
  }
  document.getElementById("shop").innerHTML = contentHTML;
};
// render cart
renderCart = (cart) => {
  var contentHTML = "";
  let total = 0;
  let cartCount = 0;
  for (i = 0; i < cart.length; i++) {
    var item = cart[i];
    var content = `<div class="cart-box">
                    <img src="${item.product.img}" alt="" class="cart-img">
                    <div class="box-content">
                        <div class="cart-product-title">${item.product.name}</div>
                        <div class="cart-product-price">$${item.product.price}</div>
                         <div class="cart-quantity" >
                            <i class='bx bxs-plus-circle' onclick = "increment(${item.product.id})"></i>
                            <span id="cart-qty">${item.quantity}</span>
                            <i class='bx bxs-minus-circle'onclick = "decrement(${item.product.id})"></i>
                        </div>
                    </div>
                    <!-- remove-cart -->
                    <i class='bx bxs-trash-alt cart-remove' onclick = "removeCart(${item.id})"></i>
                </div>`;
    contentHTML += content;
    total += cart[i].product.price * cart[i].quantity;
    cartCount += item.quantity;
  }
  document.getElementById("cart").innerHTML = contentHTML;
  document.getElementById("total-price").innerHTML = "$" + total;
  document.getElementById("cart-count").innerHTML = cartCount;
};

// Hàm lọc sản phẩm theo loại
const filterProductsByType = (products, type) => {
  if (type) {
    return products.filter((product) => product.type === type);
  } else {
    return products;
  }
};

// find item
const findItemById = (cart, id) => {
  let item;
  cart.forEach((ele) => {
    if (ele.product.id == id) {
      item = ele;
      return;
    }
  });
  return item;
};
