let cartIcon = document.querySelector("#cart-icon");
let closeCart = document.querySelector("#cart-close");
var dataJson = localStorage.getItem("cartBox");
let cartBox = [];
if (dataJson != null) {
  // convert JSON=>array
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    cartBox.push(item);
  }
}
renderCart(cartBox);

//open cart
cartIcon.onclick = () => {
  document.querySelector(".cart").classList.add("active");
};

// close cart
closeCart.onclick = () => {
  document.querySelector(".cart").classList.remove("active");
};

//render SP
fetchSP = () => {
  spService
    .getList()
    .then(function (res) {
      renderDSSP(res.data);
    })
    .catch(function (err) {
      console.log("err");
    });
};
fetchSP();

//filter SP
const productTypeSelect = document.getElementById("product-type");
// Lấy danh sách sản phẩm từ API
spService
  .getList()
  .then((res) => {
    const allProducts = res.data;
    let filteredProducts = allProducts;
    // Gắn sự kiện onchange cho dropdown filter
    productTypeSelect.addEventListener("change", () => {
      filteredProducts = filterProductsByType(
        allProducts,
        productTypeSelect.value
      );
      renderDSSP(filteredProducts);
    });
    // Khởi tạo danh sách sản phẩm ban đầu
    renderDSSP(allProducts);
  })
  .catch((error) => {
    console.error(error);
  });

// add sp into cart

const addCart = (productId) => {
  spService
    .getPhoneById(productId)
    .then(function (res) {
      const dataPhone = res.data;
      var product = new Product(
        dataPhone.id,
        dataPhone.name,
        dataPhone.price,
        dataPhone.screen,
        dataPhone.backCamera,
        dataPhone.frontCamera,
        dataPhone.img,
        dataPhone.desc,
        dataPhone.type
      );
      const newCartItem = new CartItem(product, 1);
      const cartItem = findItemById(cartBox, newCartItem.product.id);

      // Nếu sản phẩm chưa có trong giỏ hàng, thêm vào giỏ hàng
      if (!cartItem) {
        cartBox.push(newCartItem);
      }
      // Nếu sản phẩm đã có trong giỏ hàng, tăng số lượng sản phẩm lên 1
      else {
        cartItem.quantity++;
      }
      renderCart(cartBox);
      localStorage.setItem("cartBox", JSON.stringify(cartBox));
    })
    .catch(function (err) {
      console.log("err");
    });
};

// remove sp in cart
function removeCart(id) {
  var viTri = -1;
  for (var i = 0; i < cartBox.length; i++) {
    var sp = cartBox[i];
    if (sp.id == id) {
      viTri = i;
      break;
    }
  }
  cartBox.splice(viTri, 1);
  renderCart(cartBox);
}

// button tăng
let increment = (id) => {
  let cartItem = findItemById(cartBox, id);
  if (cartItem) cartItem.quantity++;
  renderCart(cartBox);
  localStorage.setItem("cartBox", JSON.stringify(cartBox));
};
// button giảm
let decrement = (id) => {
  let cartItem = findItemById(cartBox, id);
  if (cartItem) cartItem.quantity--;
  renderCart(cartBox);
  localStorage.setItem("cartBox", JSON.stringify(cartBox));
};
// clear cart
function clearCart() {
  cartBox = [];
  renderCart(cartBox);
  localStorage.removeItem("cartBox");
}
