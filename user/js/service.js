var BASE_URL = "https://64546efbc18adbbdfeb630c3.mockapi.io/products";

var spService = {
  getList: function () {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  getPhoneById: function (id) {
    return axios({
      url: `https://64546efbc18adbbdfeb630c3.mockapi.io/products/${id}`,
      method: "GET",
    });
  },
};
