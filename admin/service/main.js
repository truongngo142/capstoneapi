const BASE_URL = "https://64546efbc18adbbdfeb630c3.mockapi.io/products";
var idSelect = null;
let fetchSanPham = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      console.log(res);
      renderSP(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchSanPham();
function themSP() {
  let thongTin = layThongTinTuForm();
  let isValid =
    kiemTraRong("spName", thongTin.name) &
    kiemTraRong("spScreem", thongTin.screen);

  isValid = isValid & kiemTraRong("spPrice", thongTin.price);
  isValid =
    isValid &
    kiemTraRong("spBrand", thongTin.type) &
    kiemTraRong("spLink", thongTin.img) &
    kiemTraRong("spBackCam", thongTin.backCamera);
  isValid =
    isValid &
    kiemTraRong("spFontCam", thongTin.frontCamera) &
    kiemTraRong("spDiscrip", thongTin.desc);
  if (isValid == true) {
    axios({
      url: BASE_URL,
      method: "POST",
      data: thongTin,
    })
      .then((res) => {
        fetchSanPham();
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
function xoaSP(id) {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      fetchSanPham();
    })
    .catch((err) => {
      console.log(err);
    });
}
function suaSP(id) {
  idSelect = id;
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      document.getElementById("name").value = res.data.name;
      document.getElementById("screem").value = res.data.screen;
      document.getElementById("brand").value = res.data.type;
      document.getElementById("img").value = res.data.img;
      document.getElementById("price").value = res.data.price;
      document.getElementById("backCam").value = res.data.backCamera;
      document.getElementById("fontCam").value = res.data.frontCamera;
      document.getElementById("decs").value = res.data.desc;
    })
    .catch((err) => {
      console.log(err);
    });
}
function capNhatSanPham() {
  let thongTin = layThongTinTuForm();
  let isValid =
    kiemTraRong("spName", thongTin.name) &
    kiemTraRong("spScreem", thongTin.screen);

  isValid = isValid & kiemTraRong("spPrice", thongTin.price);
  isValid =
    isValid &
    kiemTraRong("spBrand", thongTin.type) &
    kiemTraRong("spLink", thongTin.img) &
    kiemTraRong("spBackCam", thongTin.backCamera);
  isValid =
    isValid &
    kiemTraRong("spFontCam", thongTin.frontCamera) &
    kiemTraRong("spDiscrip", thongTin.desc);
  if (isValid == true) {
    axios({
      url: `${BASE_URL}/${idSelect}`,
      method: "PUT",
      data: layThongTinTuForm(),
    })
      .then((res) => {
        fetchSanPham();
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
function sapXep() {
  let xep = document.getElementById("sapxep").value;

  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      res.data.sort(function (a, b) {
        return a.price - b.price;
      });
      if (xep == 0) {
        renderSP(res.data);
      } else {
        res.data.reverse();
        renderSP(res.data);
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

function timSanPham() {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let dataSP = res.data;
      var inPut = document.getElementById("inPutSerch").value;
      for (var i = 0; i < dataSP.length; i++) {
        if (dataSP[i].name == inPut) {
          let contentArr = `
                    <tr>
                    <th>${dataSP[i].id}</th>
                    <th>${dataSP[i].name}</th>
                    <th>${dataSP[i].price}</th>
                    <th><img src="${dataSP[i].img}" style="width:100px;height:100px;"/></th>
                    <th>${dataSP[i].desc}</th>
                    <th>
                    <button class="btn btn-primary" onclick="suaSP(${dataSP[i].id})">Sua</button>
                    <button class="btn btn-danger" onclick="xoaSP(${dataSP[i].id})">Xoa</button>
                    </th>
                    </tr>
                    `;

          document.getElementById("tablePhone").innerHTML = contentArr;
        } else {
          document.getElementById("tbTimSP").innerHTML = "Khong Tm Thay";
        }
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
